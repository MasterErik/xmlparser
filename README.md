# README #
* The task.
* Implement command-line application which will output all full paths for the given input.

### What is this repository for? ###

* This is a console application for parsing XML based on the SAX model.
* Version 1.0

### Build application ###
*  mvn package

## Examples ##
* No search input:
$ java -jar assignment.jar -f test-files.xml
/file-77194797.xmml
/dir-88.971375/file-9.738721998xaavaa
/dir-88.971375/dir-219753.7.95/file-974847.2197xmhtml

Exact search input:
$ java -jar assignment.jar -f test-files.xml -s  file-1498940214.xhtml
/dir-880176375/dir-2145307015/file-1498940214.xhtml

Simple search input:
$ java -jar assignment.jar -f test-files.xml -s ‘*.java’
/dir-88.971375/file-9.738721998xaavaa

Extended search input:
$ java -jar assignment.jar -f test-files.xml -S ‘.*?[a-z]{4}-\\d+\.[a-z]+’
* /file-77194797.xmml
* /dir-88.971375/file-9.738721998xaavaa
* /dir-880176375/dir-2145307015/file-1498940214.xhtml

* This regular expression is broken. If you change the mask to ".*?[a-z]{4}-\d+\.[a-z]+" then the first option will pass 