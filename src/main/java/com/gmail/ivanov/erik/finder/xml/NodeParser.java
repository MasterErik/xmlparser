package com.gmail.ivanov.erik.finder.xml;

import com.gmail.ivanov.erik.finder.comparator.AbstractComparator;
import com.gmail.ivanov.erik.finder.consts.XConstant;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class NodeParser extends DefaultHandler {
    private AbstractComparator comparator;
    private boolean active;
    public NodeParser(AbstractComparator comparator) {
        this.comparator = comparator;
    }

    private boolean checkElement(Attributes attributes) {
        return attributes.getLength() > 0 && attributes.getLocalName(0).equals(XConstant.IS_FILE);
    }
    private boolean checkFile(Attributes attributes) {
        return attributes.getValue(0).equals(XConstant.TRUE);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//        System.out.println("Teg open: "+qName);
        if (checkElement(attributes) ) {
            comparator.setIsFile(checkFile(attributes));
        }
        active = qName.equals(XConstant.ACTIVE_NODE);
//        super.startElement(uri, localName, qName, attributes);
    }

    @Override
    public void characters(char[] c, int start, int length) throws SAXException {
//        super.characters(c, start,  length);
        if (active) {
            String value = new String(c, start, length);
            comparator.store(value);
//            System.out.println(comparator.getCurDir() +":  "+value);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//        System.out.println("Teg close: "+qName);
        if (qName.equals(XConstant.ACTIVE_NODE)) {
            active = false;
        }

        if (qName.equals(XConstant.INCLUDE_NODE)) {
            comparator.closeDir();
        }
//        super.endElement(uri,localName, qName);
    }

    @Override
    public void startDocument() throws SAXException {
        comparator.start();
        super.startDocument();
    }


}
