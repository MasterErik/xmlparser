package com.gmail.ivanov.erik.finder.comparator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MaskComparator extends AbstractComparator {

    private Pattern pattern;

    @Override
    public void start() {
        String newMack = processingMask(getMask());
        pattern = Pattern.compile(newMack);
    }

    public String processingMask(String mack) {
        StringBuilder newMack = new StringBuilder();
        char[] chars = mack.toCharArray();
        for (char ch: chars) {
            switch (ch)
            {
                case '.'    : newMack.append("\\."); break;
                case '?'    : newMack.append("."); break;
                case '*'    : newMack.append(".*"); break;
                default     : newMack.append(ch); break;
            }
        }
        return newMack.toString();
    }

    @Override
    public boolean compare(String context) {
        Matcher m = pattern.matcher(context);
        return m.matches();
    }


}
