package com.gmail.ivanov.erik.finder.argument;

public class ArgumentException extends Exception {
    public ArgumentException(String message) {
        super(message);
    }
}
