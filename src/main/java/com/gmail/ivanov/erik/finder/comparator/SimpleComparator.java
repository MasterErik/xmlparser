package com.gmail.ivanov.erik.finder.comparator;

public class SimpleComparator extends AbstractComparator {

    @Override
    public boolean compare(String context) {
        return getMask().equals(context);
    }
}
