package com.gmail.ivanov.erik.finder.comparator;

import com.gmail.ivanov.erik.finder.consts.SearchType;

public class FactoryMethod {
    public AbstractComparator getComarator(SearchType searchType) {
        switch (searchType) {
            case Full   : return new EmptyComparator();
            case Eqals  : return new SimpleComparator();
            case Mask   : return new MaskComparator();
            case Regular: return new RegularComparator();
            default     : return new EmptyComparator();
        }
    }
}
