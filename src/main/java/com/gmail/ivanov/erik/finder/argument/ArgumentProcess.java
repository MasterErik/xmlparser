package com.gmail.ivanov.erik.finder.argument;

import com.gmail.ivanov.erik.finder.consts.SearchType;
import com.gmail.ivanov.erik.finder.consts.XConstant;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;

import static com.gmail.ivanov.erik.finder.consts.XConstant.KEY_INPUT_FILE;
import static com.gmail.ivanov.erik.finder.consts.XConstant.KEY_MACK;
import static com.gmail.ivanov.erik.finder.consts.XConstant.KEY_MACK_REGULAR;

@Getter
@Setter
public class ArgumentProcess {
    private String inputFileName;
    private String mack;
    private SearchType searchType = SearchType.Full;

    public ArgumentProcess(String[] args) throws ArgumentException, IOException {
        validator(args);
        storeParams(args);
    }

    private void validator(String[] args) throws ArgumentException {
        if (args == null || args.length < 2) {
            throw new ArgumentException("too few parameters");
        }
        if (!KEY_INPUT_FILE.equals(args[0]) ) {
            throw new ArgumentException("not supported key:" + args[0]);
        }
        if (args[1] == null || args[1].trim().equals("")) {
            throw new ArgumentException("input file doesn't exists");
        }

        if (args.length == 2 ) {
            return;
        }
        if (args.length == 3 ) {
            throw new ArgumentException("not found search parameter");
        }

        if (!(KEY_MACK.equals(args[2]) || KEY_MACK_REGULAR.equals(args[2])) ) {
            throw new ArgumentException("not supported key:" + args[2]);
        }


        if (KEY_MACK_REGULAR.equals(args[2]) || KEY_MACK.equals(args[2])) {
            if (args[3].startsWith(XConstant.APOSTROPHE1)) {
                if (args[3].length() < 3 || !args[3].substring(2).endsWith(XConstant.APOSTROPHE2)) {
                    throw new ArgumentException("not supported search mack:" + args[3]);
                }
            }
        }

    }

    private void storeParams(String[] args) throws ArgumentException, IOException {
        inputFileName = fileExists(args[1]);
        if (args.length > 2) {
            if (KEY_MACK_REGULAR.equals(args[2])) {
                mack = args[3].substring(1, args[3].length() - 1);
                searchType = SearchType.Regular;
                return;
            } else if (KEY_MACK.equals(args[2])) {
                if (args[3].startsWith(XConstant.APOSTROPHE1)) {
                    mack = args[3].substring(1, args[3].length() - 1);
                    searchType = SearchType.Mask;
                    return;
                } else {
                    mack = args[3];
                    searchType = SearchType.Eqals;
                    return;
                }
            }

            mack = args[3];
        }
    }

    private String fileExists(String fileName) throws ArgumentException, IOException {
        String path = System.getProperty("user.dir");
        File f = new File(path + File.separator + fileName);
        if(f.exists() && f.isFile()){
            return f.getAbsolutePath();
        }else{
            throw new ArgumentException("input file doesn't exists");
        }

    }
}


