package com.gmail.ivanov.erik.finder.xml;

import com.gmail.ivanov.erik.finder.comparator.AbstractComparator;

import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;

public class SaxParser {

    public SaxParser(AbstractComparator comparator, String sourceFile) throws IOException, ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();

        factory.setValidating(true);
        factory.setNamespaceAware(false);
        SAXParser parser;

        try(InputStream xmlData = new FileInputStream(sourceFile)) {
            parser = factory.newSAXParser();
            parser.parse(xmlData, new NodeParser(comparator));
        }
    }

}
