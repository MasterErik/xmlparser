package com.gmail.ivanov.erik.finder.comparator;

import com.gmail.ivanov.erik.finder.consts.XConstant;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractComparator {
    private String mask;
    private boolean isFile;
    private List<String> dir = new ArrayList<>();

    public abstract boolean compare(String context);

    public void start() {

    }

    public void store(String context) {
        if (!isFile) {
            dir.add(dir.size() > 1 ? XConstant.SPLIT_DIR + context : context);
        } else {
            dir.add("");
            if (compare(context)) {
                output(context);
            }
        }
    }

    public void output(String content) {
        log.info(getCurDir() + XConstant.SPLIT_DIR + content);
    }

    public void setIsFile(boolean value) {
        this.isFile = value;
    }
    public void closeDir() {
        dir.remove(dir.size()-1);
    }
    public String getCurDir() {
        return dir.stream()
                .filter(e -> !equals(""))
                .collect(Collectors.joining());
    }

    public void setMask(String mack) {
        this.mask = mack;
    }
    public String getMask() {
        return mask;
    }
}
