package com.gmail.ivanov.erik.finder;

import com.gmail.ivanov.erik.finder.argument.ArgumentException;
import com.gmail.ivanov.erik.finder.argument.ArgumentProcess;
import com.gmail.ivanov.erik.finder.comparator.AbstractComparator;
import com.gmail.ivanov.erik.finder.comparator.FactoryMethod;
import com.gmail.ivanov.erik.finder.xml.SaxParser;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;

@Slf4j
public class Main {

    public static void main(String[] args) {
        try {
            ArgumentProcess arguments = new ArgumentProcess(args);

            FactoryMethod factoryMethod = new FactoryMethod();
            AbstractComparator comparator = factoryMethod.getComarator(arguments.getSearchType());
            comparator.setMask(arguments.getMack());

            new SaxParser(comparator, arguments.getInputFileName());

        } catch (IOException e) {
            log.warn(e.getMessage());
            return;
        } catch (ArgumentException e) {
            log.info(e.getMessage());
            return;
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }
}
